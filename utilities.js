'use strict';

var utilities = angular.module('cc.utilities', ['ui.router', 'ui.bootstrap']);

utilities.directive('spinner', function ($window, spinner) {
    return {
        scope: true,
        link: function (scope, element, attr) {

            var window = angular.element($window);
            scope.spinner = null;

            function stopSpinner() {
                if (scope.spinner) {
                    scope.spinner.stop();
                    scope.spinner = null;
                    element.hide();
                }
            }

            function startSpinner() {
                element.show();
                scope.spinner = new $window.Spinner(angular.fromJson(attr.spinner.replace(/'/g, '\"')));
                scope.spinner.spin(element[0]);
            }

            scope.$watch(spinner.spinning, function (start) {

                if (start) {
                    startSpinner()
                } else {
                    stopSpinner();
                }
            });

            scope.$on('$destroy', function () {
                stopSpinner();
            });

            scope.style = function () {
                var top = window.height() / 2 - 35;
                var left = window.width() / 2 - 35;
                return {
                    'top': top + 'px',
                    'left': left + 'px'
                };
            }
        }
    };
});

//factory


utilities
    .factory('spinner', function () {

        var spin = false;

        // Public API here
        return {
            start: function () {
                spin = true;
            },
            stop: function () {
                spin = false;
            },
            spinning: function () {
                return spin;
            }
        };
    })
    .factory('toast', function () {
        return {
            success: function (message) {
                toastr.success(message)
            },
            error: function (message) {
                toastr.error(message);
            }
        };
    })
    .factory('cc', function (spinner, toast, $state) {

        return {
            spin: function (start) {
                if (start === true) {

                    spinner.start();
                } else {
                    spinner.stop();
                }
            },

            redirect: function (message, state, error) {
                spinner.stop();
                if (!error) {
                    toast.success(message);
                } else {
                    toast.error(message);
                }
                $state.go(state.state, state.params);
            },

            message: function (message, type) {
                if (!type || type === 'success') {
                    toast.success(message);
                }
                if (type === 'error') {
                    toast.error(message);
                }
            },

            errors: function (message, errors) {
                spinner.stop();
                message += '<ul>';
                angular.forEach(errors, function (e) {
                    message += '<li>' + e + '</li>';
                });
                message += '</ul>';
                toast.error(message);
            }


        };
    })
    .service('ccModal',
        function ($uibModal) {

            var modalDefaults = {
                backdrop: true,
                keyboard: true,
                modalFade: true,
                template: '<div class="modal-header"> <h3 class="modal-title">{{modalOptions.headerText}}</h3></div><div class="modal-body"> <p ng-bind-html="modalOptions.bodyText"></p></div><div class="modal-footer"><button type="button" class="btn btn-default" data-ng-click="modalOptions.close()">{{modalOptions.closeButtonText}}</button> <button class="btn btn-primary" ng-show="modalOptions.actionButtonText != \'\'" data-ng-click="modalOptions.ok();">{{modalOptions.actionButtonText}}</button> </div>'
            };

            var modalOptions = {
                closeButtonText: 'Close',
                actionButtonText: 'OK',
                headerText: 'Proceed?',
                bodyText: 'Perform this action?'
            };

            this.showModal = function (customModalDefaults, customModalOptions) {
                if (!customModalDefaults) {
                    customModalDefaults = {};
                }
                customModalDefaults.backdrop = 'static';
                return this.show(customModalDefaults, customModalOptions);
            };

            this.show = function (customModalDefaults, customModalOptions) {
                //Create temp objects to work with since we're in a singleton service
                var tempModalDefaults = {};
                var tempModalOptions = {};

                //Map angular-ui modal custom defaults to modal defaults defined in service
                angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

                //Map modal.html $scope custom properties to defaults defined in service
                angular.extend(tempModalOptions, modalOptions, customModalOptions);

                if (!tempModalDefaults.controller) {
                    tempModalDefaults.controller = ["$scope", "$uibModalInstance", function ($scope, $uibModalInstance) {
                        $scope.modalOptions = tempModalOptions;
                        $scope.modalOptions.ok = function (result) {
                            $uibModalInstance.close(result);
                        };
                        $scope.modalOptions.close = function () {
                            $uibModalInstance.dismiss('cancel');
                        };
                    }];
                }

                return $uibModal.open(tempModalDefaults).result;
            };

            this.showAlert = function(alertMessage) {

                // Simplified version to show modal message and wait for click on OK
                var tempModalDefaults = {};
                var tempModalOptions = {};

                angular.extend(tempModalDefaults, modalDefaults, {backdrop: 'static'});
                angular.extend(tempModalOptions, modalOptions, {closeButtonText: 'Ok', actionButtonText: '',headerText: 'Commissary Connect', bodyText: alertMessage});

                if (!tempModalDefaults.controller) {
                    tempModalDefaults.controller = ["$scope", "$uibModalInstance", function ($scope, $uibModalInstance) {
                        $scope.modalOptions = tempModalOptions;
                        $scope.modalOptions.ok = function (result) {
                            $uibModalInstance.close(result);
                        };
                        $scope.modalOptions.close = function () {
                            $uibModalInstance.dismiss('cancel');
                        };
                    }];
                }

                return $uibModal.open(tempModalDefaults).result.catch(function(res) {
                    if (!(res === 'cancel' || res === 'escape key press')) {
                        throw res;
                    }
                });

            };

        });